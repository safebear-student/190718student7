package com.safebear.app;

import com.safebear.app.pages.HomePage;
import com.safebear.app.pages.LoginPage;
import com.safebear.app.pages.UserPage;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.Utils;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    WebDriver driver;
    Utils utility;
    HomePage homePage;
    LoginPage loginPage;
    UserPage userPage;

    @Before
    public void setUp() {

        utility = new Utils();
        //driver = new ChromeDriver();

        //ChromeOptions chromeOptions = new ChromeOptions();
        //chromeOptions.addArguments("headless");
        //this.driver = new ChromeDriver(chromeOptions);
        driver = utility.getDriver();

        homePage = new HomePage(driver);
        loginPage = new LoginPage(driver);
        userPage = new UserPage(driver);

        driver.get(utility.getUrl());
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();

    }
}
