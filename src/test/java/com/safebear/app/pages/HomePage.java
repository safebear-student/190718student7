package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {

    WebDriver driver;
    @FindBy(linkText = "Login")
    WebElement loginlink;


    // contructor for the page
    public HomePage(WebDriver driver)
    {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public Boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Welcome");
    }

    public void clickLoginLink() {
        loginlink.click();
    }
}
