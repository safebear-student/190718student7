package com.safebear.app;

import com.safebear.app.pages.UserPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {
    @Test
    public void testLogin () {
        //Step 1 confirm we're on the home page
        assertTrue(homePage.checkCorrectPage());
        //step 2 Click on login link
        homePage.clickLoginLink();
        //step 3 confirm we're on the login page
        assertTrue(loginPage.checkCorrectPage());
        //step 4 Login with valid creds
        loginPage.login("testuser","testing");
        //Step 5 Check we're on the User Page
        assertTrue(userPage.checkCorrectPage());

    }
}
